package ir.arbn.www.mysematecprojectfour;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Created by A.R.B.N on 2/5/2018.
 */

public class FriendsListAdapter extends BaseAdapter {
    Context mContext;
    String Names[];

    public FriendsListAdapter(Context mContext, String[] names) {
        this.mContext = mContext;
        Names = names;
    }

    @Override
    public int getCount() {
        return Names.length;
    }

    @Override
    public Object getItem(int i) {
        return Names[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View row = LayoutInflater.from(mContext).inflate(R.layout.adapter_list_friends, null);
        TextView Name = row.findViewById(R.id.Name);
        Name.setText(Names[i]);
        return row;
    }
}
