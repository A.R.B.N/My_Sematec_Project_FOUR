package ir.arbn.www.mysematecprojectfour;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {
    ListView MyList;
    Context mContext = HomeActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        MyList = findViewById(R.id.MyList);
        String Names[] = {
                "AhmadReza",
                "Mahsa",
                "Amir",
                "Mina",
                "Aida",
        };
        FriendsListAdapter Adapter = new FriendsListAdapter(mContext, Names);
        MyList.setAdapter(Adapter);
        MyList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String ClickedName = (String) adapterView.getItemAtPosition(i);
                Toast.makeText(mContext, ClickedName, Toast.LENGTH_SHORT).show();
            }
        });
    }
}